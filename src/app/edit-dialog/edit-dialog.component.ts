import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CakeModel } from '../panel/panel.component';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {
  cake: CakeModel = {
    title: '',
    description: '',
    numberOfPortions: null,
    price: null,
    img: ''
  };
  formControllers = {
    title: new FormControl('', [
      Validators.required,
    ]),
    description: new FormControl('', [
      Validators.maxLength(250)
    ]),
    numberOfPortions: new FormControl('', [
      Validators.required,
    ]),
    price: new FormControl('', [
      Validators.required,
    ]),
    img: new FormControl('', [
      Validators.required,
    ])
  };

  portions = [4, 8, 12];
  hasValidationErr: boolean;

  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.hasValidationErr = false;


    for (const key in this.formControllers) {
      if (this.formControllers.hasOwnProperty(key)) {
        const element = this.formControllers[key];
        if (!element.valid && key !== 'img') {
          this.hasValidationErr = true;
        } else if (key === 'img' && !this.cake.img) {
          this.hasValidationErr = true;
        }
      }
    }

    if (!this.hasValidationErr) {
      const cake = { ...this.cake };
      this.dialogRef.close({ cake });
    }


  }

  editImage(imageInput): void {
    const files = imageInput.files;
    const reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onload = e => {
      this.cake.img = reader.result;
    };
  }

  changePortion(item) {
    if (item) {
      this.cake.numberOfPortions = item;
    }
  }

  ngOnInit() {
    if (this.data.cake) {
      this.cake = { ...this.data.cake };

      for (const key in this.data.cake) {
        if (this.data.cake.hasOwnProperty(key) && key !== 'img') {
          this.formControllers[key].setValue(this.data.cake[key]);
        }
      }

    }
  }

}

