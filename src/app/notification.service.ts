import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export interface NotificationModel {
  content: string;
}

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notificationSubject = new Subject<NotificationModel>();

  constructor() { }


}
