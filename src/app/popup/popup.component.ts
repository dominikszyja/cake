import { Component, OnInit } from '@angular/core';
import { style, state, animate, transition, trigger } from '@angular/animations';
import { NotificationService, NotificationModel } from '../notification.service';
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('show', style({
        opacity: 1
      })),
      state('hide', style({
        opacity: 0
      })),
      transition('show => hide', animate('500ms ease-out')),
      transition('hide => show', animate('300ms ease-in'))
    ])
  ]

})
export class PopupComponent implements OnInit {
  public content = '';
  public status = 'hide';
  public isHidden = true;

  constructor(private notificationService: NotificationService) {
  }

  toggle() {
    this.status = 'show';
    this.isHidden = false;
    setTimeout(() => {
      this.status = 'hide';
      setTimeout(() => {
        this.isHidden = true;
      }, 300);
    }, 1500);
  }


  ngOnInit() {
    this.notificationService.notificationSubject.subscribe((x: NotificationModel) => {
      if (x) {
        this.content = x.content;
        this.toggle();
      }
    });

  }

}
