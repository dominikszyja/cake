import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { NotificationService } from '../notification.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { StorageService } from '../storage.service';

export interface CakeModel {
  img: any;
  title: string;
  description: string;
  numberOfPortions: number;
  price: number;
}

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  constructor(public dialog: MatDialog,
    private notificationService: NotificationService,
    private storageService: StorageService) { }


  cakeArr: Array<CakeModel> = [{
    img: './assets/cake_01.png', title: 'Ciasto czekoladowe',
    description: 'Ciasto z lekkim musem truskawkowym i bitą śmietaną, na czekoladowym spodzie.',
    numberOfPortions: 4, price: 40
  },
  {
    img: './assets/cake_02.png', title: 'Ciasto o smaku chałwy',
    description: 'Smak chałwy połączony z delikatnym kremem śmietankowym i chrupkimi wafelkami.',
    numberOfPortions: 12, price: 100
  },
  {
    img: './assets/cake_03.png', title: 'Ciasto czekoladowo-kawowe',
    description: 'Intensywny, czekoladowy smak biszkoptu połączony z kawowymi i karmelowymi masami.',
    numberOfPortions: 8, price: 90
  },
  {
    img: './assets/cake_04.png', title: 'Ciasto wiśniowe',
    description: 'Intensywny, czekoladowy smak biszkoptu połączony z wiśniową konfiturą i czekoladowym kremem.',
    numberOfPortions: 4, price: 60
  }];

  ngOnInit() {
    const storageData = this.storageService.get('cakeArr');
    if (storageData) {
      this.cakeArr = storageData;
    }

  }

  edit(cakeIndex): void {

    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '630px',
      // height: '665px',
      // height: 'calc(100% - 245px)',
      data: { cake: cakeIndex || cakeIndex === 0 ? this.cakeArr[cakeIndex] : null },
      panelClass: 'edit-dialog',
      backdropClass: 'edit-shadow'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.cake) {
        if (!this.cakeArr[cakeIndex]) {
          this.notificationService.notificationSubject.next({ content: 'Dodałeś nowe ciasto!' });
        }
        if (cakeIndex || cakeIndex === 0) {
          this.cakeArr[cakeIndex] = { ...result.cake };
        } else {
          this.cakeArr.push({ ...result.cake });
        }
      }
      this.storageService.set('cakeArr', this.cakeArr);
    });

  }

  remove(cakeIndex): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      // panelClass: 'edit-dialog',
      // backdropClass: 'edit-shadow',
      width: '630px',

    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && 'isValid' in result && result.isValid) {
        this.notificationService.notificationSubject.next({ content: 'Usunąłeś ciasto' });
        this.cakeArr.splice(cakeIndex, 1);
        this.storageService.set('cakeArr', this.cakeArr);
      }
    });
  }

}
