import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  get(key): any {
    return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null;
  }

  set(key, data): void  {
    localStorage.setItem(key, JSON.stringify(data));
  }
}
