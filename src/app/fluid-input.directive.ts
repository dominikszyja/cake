import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appFluidInput]'
})
export class FluidInputDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.renderer.setStyle(
      this.el.nativeElement,
      'width',
      1 + 'ch'
    );
  }


  @HostListener('keyup', ['$event.target'])
  onValue(event) {

    if (event.value.length) {
      this.renderer.addClass(this.el.nativeElement.parentNode, 'touched');
    } else {
      this.renderer.removeClass(this.el.nativeElement.parentNode, 'touched');
    }
    
    const len = event.value.length ? event.value.length : 1;
    this.renderer.setStyle(
      this.el.nativeElement,
      'width',
      len + 'ch'
    );


  }


}
